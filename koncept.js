(function (global) {
  "use strict";

  function As(fn, args, limit,  cb) {
    this.limit = limit;
    this.fn = fn;
    this.items = args;
    this.final = cb;
    this.results = [];
    this.done = 0; //number of done tasks
    this.idx = 0; //index of last started task
  }
  
  As.prototype.getRunning = function getRunning() {
    return this.idx - this.done;
  };
  
  As.prototype.loger = function loger(idx, action) {
    //log
    console.log(action, idx + 1,'. task of', this.items.length, 'running:',
      this.getRunning(), 'done:', this.done);
  };

  As.prototype.ucb = function ucb(args, idx) {
    var arar = Array.prototype.slice.call(args);
    
    //save result
    if(args.length > 1) {
      this.results[idx] = arar;
    } else if (args.length === 1) {
      this.results[idx] = arar[0];
    }
    
    this.done++; //one is done
    this.loger(idx, '<-DONE->');
    
    //recursion or done if last index num.
    var last = this.items.length - 1;
    if((this.done < last) && (this.getRunning() > 0)) {
      this.run();
    } else if(this.getRunning() === 0 && this.idx === this.items.length) {
      this.done = this.idx = 0; //reset
      this.final(this.results);
    }
  };

  As.prototype.run_one = function run_one(fn, args, idx) {
    //TODO args neni 1 arg ale pole argunebtů
    //parsovat a vstriknout original cb, kdyz je...
    this.loger(idx, '<-STARTED->');
    var self = this;
    //fn.apply(args);
    fn(args, function() {
      self.ucb(arguments, idx);
    });
  };

  As.prototype.run = function run() {
    //fill parallell run buffer
    while((this.idx < this.items.length) && (this.getRunning() <= this.limit)) {
      var args = this.items[this.idx];
      this.run_one(mult, args, this.idx);
      this.idx++;
    }
  };
  
  global.As = As;
})(this);

/////////////////////////////////////////////////////////////////////
//Task se udela jako
//
/*global As*/
function mult(arg, callback) {
  //var rt = arg * 100;
  var rt = Math.round(Math.random() * 500);
  console.log('Process of \''+arg+'\', arg and return after ' + rt + ' sec.');
  setTimeout(function() {
    callback(arg * 2); 
  }, rt);
}
var args = [ 1, 2, 3, 4, 5, 6 ];
function final(results) { console.log('Done all:', results); }

var as = new As(mult, args, 3, final);
as.run();