var fs = require('fs');
var path = require('path');
var proc = process;

var mgen = {};
//determine base categories, aliases and subcategories
mgen.BASE_CATS = require('./basecat');
var templates = require('./templates');

mgen.DESKTOP_DIRS = mgen.DESKTOP_DIRS || '/usr/share/desktop-directories';
mgen.DESKTOP_DIRS_PREFIX = mgen.DESKTOP_DIRS_PREFIX || 'Arch';
mgen.ICON_SIZE_MAX = 256;
mgen.ICON_THEME_DIR = mgen.ICON_THEMES_DIR || '/usr/share/icons';
mgen.ICON_THEME_NAME = mgen.ICON_THEME_NAME || 'hicolor';
mgen.LANG = proc.env.LANG.split('_')[0];
mgen.PLIMIT = 50;


mgen.find = function(dir, opts, cb) {
  cb = arguments[arguments.length - 1];
  opts = typeof opts === 'object' ? opts : {};
  opts.recursive = opts.recursive || false;
  opts.onlyfiles = opts.onlyfiles || false; //only files filter
  
  
  var  results = (opts.ext || opts.rgx || opts.onlyfiles) ? [] : [dir];
  
  //main recursive loop
  fs.readdir(dir, function(err, list) {
    if (err) {
      return cb('Error read dir: "' + dir + '" : "' + err + '" !', null);
    }
    
    var remain = list.length; //number of remained
    
    if (list.length === 0) { //is empty dir -> end
      return cb(false, results);
    }
    
    list.forEach(function(file) {
      file = path.join(dir,file); //get full path
      
      //test is file / directory
      fs.stat(file, function(err, stat) {
        remain --;
        if (err) {
          return cb('Error read file: "' + file + '" :' + err, null);
        }
        
        //directories ->
        if (stat && stat.isDirectory()) {
          if (opts.recursive) {
            mgen.find(file, opts, function(err, res) {
              results = results.concat(res);
              if (remain === 0) {
                cb(false, results);
              } //end
            });
          //no recursive and opts.onlyfiles === false
          } else if (!opts.onlyfiles) {
            results.push(file);
          }
        //files ->
        } else if (stat && stat.isFile()) {
          var filtered = false;
          var no_ext = (opts.ext && (path.extname(file) !== ('.' + opts.ext)));
          var no_rgx = (opts.rgx && !(file.match(opts.rgx)));
          
          if (no_ext || no_rgx) {
            filtered = true;
          }
          //filtering results
          if (!filtered) {
            results.push(file);
          }
          if (remain === 0) cb(false, results); //end
        }
      });
    });
  });
};

//get file list .directory files for prefix
mgen.getDesktopDirFiles = function getDesktopDirFiles(prefix, cb) {
  mgen.find(mgen.DESKTOP_DIRS, {rgx: new RegExp(prefix), ext: 'directory'}, function(err, list) {
    if(err) {
      cb('Error when find desktop directories:' + err, null);
    }
    if (list.length === 0) {
      cb('No desktop category directories found use prefix: ' + prefix, null);
    } else {
      cb(false, list);
    }
  });
};

//get base Categories from mgen.DESKTOP_DIRS using mgen.DESKTOP_DIRS_PREFIX
mgen.parseDesktopDirs = function parseDesktopDirs(prefix, idata, cb) {
  //find desktop .directory files using prefix string
  mgen.getDesktopDirFiles('Arch', function(err, files) {
    if(err) return cb(err, null);
    //parse ddata from this files
    mgen.parseDesktopFiles(files, idata, function(err, cdata) {
      if(err) return cb(err, null);
      //TODO add icons pathes
      cb(false, cdata);
    });
  });
};

//parse one .desktop or .directory file using idata
mgen.parseDesktop = function parseDesktop(file, idata, cb) {
  var obj = {
    base: {},
    i18n: {}
  };
  
  fs.readFile(file, function(err, data) {
    if (err) {
      cb('Error reading file ' + file + ' for parse DesktopEntry key values: ' + err, null);
    }
    var lines = data.toString().split('\n');
    //search keys
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i];
      var kv = line.split('=');
      // = split key/val
      if (kv.length === 2) {
        var key = kv[0].trim();
        var val = kv[1].trim();
        //capture i18n for selected or detected mgen.LANG language code
        var key_i18n = key.match(/^([\w\W]+)\[([\w\W]+)\]$/); //test i18n key
        //validate and parse international key <key[i18n]=...>
        if(key_i18n && key_i18n[1] && key_i18n[2]) {
          if((key_i18n[2] === mgen.LANG)) {
            var lkey = key_i18n[1]; //key value without [i18n]
            obj.i18n[lkey] = val;
          }
        } else { //universal no internatiolnal value
          obj.base[key] = val;
        }
      } else if (kv.length === 1 && kv[0].match(/\[Desktop Entry\]/)) {
        obj.isDesktop = true;
      }
    }
    //validete [Desktop Entry]
    if(obj.isDesktop) {
      delete obj.isDesktop; //marker not usefull
      //insert icons absolute pathes
      mgen.getIcons(obj.base.Icon, idata, function(err, icons) {
        if(err) return cb('Error of adding icon path: ' + err, null);
        if(icons && (icons.bitmap[0]||icons.scalable[0])) {
          console.log('ADDED ICONS', icons);
          obj.icons = icons;
        } else { //TODO define mgen.FALLBACK_ICON_FILE
          console.log('FALLBACK ICON!!', obj.base.Icon, icons);
          obj.icons = mgen.FALLBACK_ICON_FILE;
        }
        cb(false, obj);
      });
    } else {
      cb('Parse desktop file failed - missing "[Desktop Entry]" line!', null);
    }
  });
};

//insert absolute icon pathes to data parsed from .desktop or .FDIRectory files
mgen.getIcons = function getIcons(iconame, idata, cb) {
  var icons = {
    bitmap: [],
    scalable: []
  };
  
  var running = 0;
  var onefile = function iterator() {
    while((running < mgen.PLIMIT) && (idata.length > 0)) {
      var icopath = idata.shift();
      var icoext;
      //scalable <num>x<num> dir -> png
      if (icopath.split('/')[0].match(/^\d+x\d+$/)) {
        icoext = 'png';
      //scalable -> svg
      } else {
        icoext = 'svg';
      }
      var icofile = path.join(mgen.ICON_THEME_DIR, mgen.ICON_THEME_NAME, icopath, iconame) +
        '.' + icoext;
      //test icofile exists
      //console.log('TEST ICOFILE', icofile);
      fs.stat(icofile, function(err, stat) {
        running--;
        if(stat && stat.isFile()) {
          //console.log('IKONA', icofile, stat);
          if(icoext === 'png') {
            //console.log('PUSH PNG ICON', icofile);
            icons.bitmap.push(icofile);
          } else if (icoext === 'svg') {
            //console.log('PUSH SVG ICON', icofile);
            icons.scalable.push(icofile);
          }
        }
        //continue recursion
        if(idata.length > 1) {
          onefile();
        //end of recursion and run cb
        } else if (running === 0) {
          //console.log('DDATA ICONS', icons);
          cb(false, icons);
        }
      });
      running++;
    }
  };
  onefile();
};

//parse icon dirs from <mgen.ICON_THEME_DIR>/index.theme file
mgen.parseIconThemeDirs = function parseIconThemeDirs(cb) {
  var theme = path.join(mgen.ICON_THEME_DIR, mgen.ICON_THEME_NAME, 'index.theme');
  var idata = [];
  fs.readFile(theme, function(err, data) {
    if (err) return cb('Failed reading index icon theme file ' + theme + ' error: ' + err);
    var lines = data.toString().split('\n');
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i];
      var kv = line.split('=');
      //parse directories
      if((kv.length > 1) && (kv[0] === 'Directories')) {
        var idirar = kv[1].trim().split(','); //icon dir
        for (var j = 0; j < idirar.length; j++) {
          //filter empty
          var relpath = idirar[j];
          if ((typeof relpath === 'string') && (relpath.length > 0)) {
            relpath = relpath.trim(); //no whitespaces
            idata.push(relpath); //add absolute path
          } 
        }
      }
    }
    cb(false, idata);
  });
}; 

//parse multiple .desktop and .directory files using parseDesktop
mgen.parseDesktopFiles = function parseDesktopFiles(filear, idata, cb) {
  var dirs = [];
  var running = 0;
  
  var onefile = function iterator() {
    while((running < mgen.PLIMIT) && (filear.length > 0)) {
      var file = filear.shift();
      mgen.parseDesktop(file, idata, function(err, obj) {
        running--;
        if(err) {
          dirs.push(new Error(err));
          //console.log('DESKTOP FILE ERROR', err);
        } else if (typeof obj === 'object') {
          //console.log('DESKTOP OBJ', obj);
          dirs.push(obj);
        }
        //continue recursive
        if(filear.length > 1) {
          onefile();
        //end of recursion and run cb
        } else if (running === 0) {
          cb(false, dirs);
        }
      });
      running++;
    }
  };
  onefile();
};

//determine base category for .desktop file data using BASE_CATS spec
//return array -> first element is BASE_CAT name and next is aliases
//if need, use subs subcategories for determining base category
mgen.determineBaseCatName = function determineBaseCatName(catar) {
  console.log('CATAR', catar);
  var bcat = []; //deermining own BASE_CATS
  for (var bcatname in mgen.BASE_CATS) {
    var bcatob = mgen.BASE_CATS[bcatname];
    //base category object name is in catar
    if(catar.indexOf(bcatname) !== -1) {
      bcat = bcat.concat([bcatname], bcatob.aliases);
    } else {
      //search relevant catar element in bcatob.aliases
      for(var i = 0; i < bcatob.aliases.length; i++) {
        var alias = bcatob[i]; //base category alias
        //any catar element is found in bcatob.aliases
        if(catar.indexOf(alias) !== -1) {
          bcat = bcat.concat([bcatname], bcatob.aliases);
        }
      }
    }
  }
  
  //if bcat not determined, next use subs for heuristic
  if (!bcat[0]) {
    var rating = {};
    var cratename = 'Other'; // rate value 0 -> base category 'Other'
    var crateval = 0;
    //testing all cats in catar 
    for (var catname in catar) {
      var dcat = catar[catname];//one word in catar
      if(dcat) { //no empty
        for(var sbcatname in mgen.BASE_CATS) { //test any BASE_CAT obj
          var sbcatob = mgen.BASE_CATS[sbcatname];
          var subs = sbcatob.subs; //subcategory names
          if(subs.indexOf(dcat) !== -1) { //if is in dcat rate it..
            rating[sbcatname] = rating[sbcatname] ? rating[sbcatname] + 1 : 1;
          }
        }
        //select best ratting object if any found
        for (var roname in rating) {
          var rate = rating[roname]; //one rate value
          if(rate > crateval) {
            cratename = roname; //update name
            crateval = rate; //updae rate
          }
        }
      }
    }
    bcat = ((cratename === 'Other') && (crateval === 0)) ? [cratename] :
      bcat.concat([cratename], mgen.BASE_CATS[cratename].aliases);
  }
  return bcat; 
};

//find base category name for all ddata objects and insert in
mgen.insertCategory = function insertCategory(ddata, cdata) {
  for(var i = 0; i < ddata.length; i++) {
    var daob = ddata[i];
    //fallback if not found "Categories="
    var dcats = daob.base.Categories || 'Other';
    dcats = dcats.split(';');

    //clearing -> select non empty string type dcats
    var catar = [];
    for(var j = 0; j < dcats.length; j++) {
      var rawcat = dcats[j];
      if ((typeof rawcat === 'string') && (rawcat.length > 0)) {
        var trimmed = dcats[j].trim();
        if(trimmed) {
          catar.push(dcats[j].trim());
        }
      }
    }
    
    //get result determined base category names
    var basecats = mgen.determineBaseCatName(catar);
    
    //insert matched caegory object -> one of basecats
    
    var fallback = null; //fallback "Other" cdata category object
    for (var k = 0; k < cdata.length; k++) {
      var dbcat = cdata[k];
      //base vategory found
      if(basecats.indexOf(dbcat.base.Name) !== -1) {
        daob.baseCategory = dbcat;
      } else if (dbcat.base.Name === 'Other') {
        fallback = dbcat;
      }
    }
    //fallbacks
    if (!daob.baseCategory) {
      daob.baseCategory = fallback;
    }
  }
};

//parse desktop files using parent directory
mgen.getDesktopData = function getDesktopData(dir, cb) {
  
  mgen.parseIconThemeDirs(function(err, idata) {
    //console.log('ICON DATA', idata);
    //find desktop files in dir recursive
    mgen.find(dir, {recursive: true, ext: 'desktop'}, function(err, filear) {
      if (err) return cb(err, null);
      console.log('PARSE DESKTOP FILES:', filear);
      //console.log('IDATA', idata);
      //parse found .desktop to dfdata object and + insert icon pathes data
      mgen.parseDesktopFiles(filear, idata, function(err, dfdata) {
        if (err) return cb(err, null);
        console.log('DIR DATA', dfdata, 'IDATA', idata);
        //get cdata (category data from .directory files) + category icon pathes data
        mgen.parseDesktopDirs(mgen.DESKTOP_DIRS_PREFIX, idata, function(err, cdata) {
          if (err) return console.log(err);
          console.log('CATEGORY .directory DATA:', cdata, 'IDATA', idata);
          //insert base category in all dfdata objects
          mgen.insertCategory(dfdata, cdata);
          cb(false, dfdata);
        });
      });
    });
  });
};

module.exports = mgen;