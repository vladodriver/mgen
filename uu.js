"use strict";
Function.prototype.cor = function cor(max) {
  var cin = 0; //current iteration
  var done = false; //done state
  var cresult; //current returned result
  var now = Date.now || new Date().getTime;
  
  var result = function result(start, dtime) {
    if (cin === max) {
      done = true;
    }
    //tttt
    return {
      cstart: start,
      cin: cin,
      of: max,
      done: done,
      time: dtime,
      result: cresult //for any object returned as result
    };
  };
  var cr = this;
  var args = arguments;
  return {
    count: function count(c) {
      var started = now();
      var pn = ((cin + c) > max) ? max : (cin + c);
      while(cin < pn) {
        cresult = cr.apply(args); //run repeat fn
        cin++; //increment/decrement counter
      }
      return result(started, now() - started);
    },
    time: function time(t) {
      var started = now();
      while(((now() - started) < t) && (cin < max)) {
        cresult = cr.apply(args);
        cin++;
      }
      return result(started, now() - started);
    },
    status: function status() {
      return {
        cin: cin,
        of: max,
        done: done,
        result: cresult || null
      };
    }
  };
};