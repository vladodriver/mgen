(function () {
  "use strict";

  //one task -> packed function with params
  function Task(fn, params) {
    if (typeof fn !== 'function') {
      throw new TypeError('Task passed at first parameter must be Function!');
    }
    if (typeof params !== 'object' || (params.constructor.name !== 'Array')) {
      throw new TypeError('Params passed at second parameter must be Array!');
    }
    this.fn = fn;
    this.params = params;
    this.queue = null; //change on added task changed to link of Async.tasks array
    
    this.done = false;
    this.status = null; //null -> ready -> running -> error || ok (at done action)
  }

  //async task manager -> run every task and save results
  function Async(options, donecb) {
    this.options = (typeof options === 'object') ? options : {};
    this.options.maxParalel = (this.options.maxParalel > 0) ? this.options.maxParalel : 50;
    this.options.debug = (this.options.debug === true) ? true : false;
    this.options.emergencyStop = (this.options.emergencyStop === true) ? true : false;
    
    this.init(); //get initial state
    return this;
  }
  
  //reset on initial state
  Async.prototype.init = function init() {
    this.tasks = [];
    this.child = null; //link to child async level
    this.parent = null; //link to parent async level
    this.level = 0; //level of task tree
    this.logs = [];
    this.results = [];
    this.status = {
      done: 0,
      count: 0,
      running: 0,
      failed: 0
    };
  };
  
  //add one task
  Async.prototype.add = function add(fn, params) {
    fn = arguments[0];
    params = Array.prototype.slice.call(arguments, 1);
    //insert new task to queue
    var task = new Task(fn, params);
    task.queue = this.tasks; //link to own tasks array
    var idx = this.tasks.push(task) - 1; //get last index of task
    task.index = idx; //task index or tasks array
    this.injectUacb(idx); //inject or add uacb callback for save results
    this.status.count++; //++ queue length
    task.status = 'ready'; //task is ready after inject uacb
    this.logAction(idx); //log add new pack
    return this;
  };
  
  //run one task index of idx
  Async.prototype.runTask = function runTask(idx) {
    var task = this.tasks[idx];
    var logob = this.logs[idx] = {};
    //create log object
    logob.started = this.time();
    //make task callback
    var self = this;
    task.fn.apply(self, task.params);
    task.status = 'running';
    this.status.running++;
    this.logAction(idx); //log run event
  };
  
  Async.prototype.time = function time(t) {
    var ts = new Date().getTime();
    return (t > 0) ? (ts - t) : ts;
  };
  
  Async.prototype.then = function then(options) {
    if (this.child === null) {
      this.child = new Async(options); //create next level -> new instance
      this.child.level = this.level + 1; //update level num
      this.child.parent = this; //link to parent
    }
    return this.child;
  };
  
  //save arguments to idx of results and create logs
  Async.prototype.saveResult = function saveResult(idx, args) {
    //save results
    this.results[idx] = args;
    this.tasks[idx].done = true;
    var err = this.getError(args);
    this.status.running--; //parallel running counter
    this.status.done++; //done counter
    if(err) {
      this.tasks[idx].status = 'error';
      this.status.failed++; //error counter
    } else {
      this.tasks[idx].status = 'ok';
    }
    this.logAction(idx); //log done event
    
    //check when all tasks done and done and run next async tasks level
    if ((this.status.done === this.status.count) && (this.status.running === 0)) {
      
    }
  };
  
  //replace or insert uacb callback fn for saving results
  Async.prototype.injectUacb = function injectUacb(idx) {
    var task = this.tasks[idx];
    var params = task.params;
    var self = this;
    var last_param = params[params.length - 1];
    
    var origcb = (typeof last_param === 'function') ? last_param : null;
    //insert base unviversal async. callback and inject original cb
    var uacb = function uacb() {
      //inject orig. cb from closure
      if (origcb) {
        last_param.apply(self, arguments);
      }
      self.saveResult(idx, arguments);
    };
    
    if (origcb) {
      //replace original cb with base unviversal async. callback
      task.params[task.params.length - 1] = uacb;
    } else {
      //add base unviversal async. callback
      params.push(uacb);
    }
  };
  
  //get level 0 instance
  Async.prototype.getRoot = function getRoot() {
    var root = this;
    //iterate to root (async level 0) instance
    while(root.level > 0) {
      root = root.parent;
    }
    return root;
  };
  
  //search Error object in self.done results 
  Async.prototype.getError = function getError(args) {
    for (var i = 0; i < args.length; i++) {
      var arg = args[i];
      if (typeof arg === 'object' && typeof arg.stack !== 'undefined' &&
        arg.constructor.name.match(/Error/)) {
        return arg;
      }
    }
    return false;
  };
  
  //inject own final callback if any and run queue empty actions
  Async.prototype.udonecb = function udonecb() {
    
  };
  
  //save log one action and print to console on debug mode
  Async.prototype.logAction = function logAction(idx) {
    var task = this.tasks[idx];
    var logob; //use existing log object
    var msg = 'Parallel tasks level: ' + this.level + ', task num: ' + idx + ', name: ' +
      (task.fn.name || 'anonymous');
    var type; // msg || error
    
    //determine started or ended task log action
    if (task.status === 'ready' && task.done === false) {
      //create logob for new added task
      logob = (typeof this.logs[idx] === 'object') ?  this.logs[idx] : {};
      logob.added = this.time();
      msg += ' added to queue.';
      type = 'message';
    } else if (task.status === 'running' && task.done === false) {
      logob.started = this.time();
      msg += 'started.';
      type = 'message';
    } else if (task.done === true) {
      //search Error object in results
      var err = this.getError(this.results[idx].results);
      var errmsg = err ? ' with error: "' + err.message + '" !' : ' OK.';
      logob.done = this.time(task.started);
      type = (task.status === 'error') ? 'error' : 'message';
      msg += 'done ' + errmsg;
    }
    
    this.logs[idx] = logob; //update logob at idx
    this.log(type, msg); //log action if options.debug is true
  };
  
  //print log to console, if options.debug = true
  Async.prototype.log = function log(type, msg) {
    if(this.options.debug) {
      var lfn;
      if(type === 'message') {
        lfn = 'log';
      } else if (type === 'error') {
        lfn = 'error';
      }
      var dt = new Date();
      var tstr = dt.getDate() + '.' + (dt.getMonth() + 1) + '.' + dt.getFullYear() + '-' +
        dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds() + '.' + dt.getMilliseconds();
      console[lfn]('Async: ' + tstr + ' -> ' + msg);
    }
  };
  
  //run any fn to next tick
  Async.prototype.tick = (function _getTickFn() {
    var itfn; //immediate tick function
    if(typeof setImmediate === 'function') { //nodejs or browsers with setImmediate
      itfn = setImmediate;
    } else if (typeof process === 'object' && typeof process.nextTick === 'function') {
      itfn = process.nextTick;
    } else if (typeof window === 'object' && typeof postMessage === 'function') {
      itfn = function pmtick(cb) {
        var id = Math.random().toString().slice(2);
        var args = [].slice.call(arguments, 1);
        window.addEventListener('message', function(e) {
          if(e.data === id) {
            cb.apply(this, args);
          }
        });
        window.postMessage(id, '*');
      };
    } else if (typeof document === 'object' && typeof document.createElement === 'function') {
      itfn = function imtick(cb) {
        var args = [].slice.call(arguments, 1);
        var img = document.createElement('img');
        img.addEventListener('error', function() {
          cb.apply(this, args);
        });
        img.src = '///';
      };
    } else { //fallback setTimeoot(0);
      itfn = function sttick(cb) {
        var args = [].slice.call(arguments, 1);
        setTimeout(function() {
          cb.apply(this, args);
        }, 0);
      };
    }
    return itfn;
  })();
  
  //exports nodejs or browser...
  if (typeof window === 'object') { //browser
    window.Task = Task;
    window.Async = Async;
  } else if (typeof module === 'object') { //nodejs
    module.exports = {
      Task: Task,
      Async: Async
    };
  }

})();

/*global Async*/
var as = new Async({debug: true});

function asad(a, b, cb) {
  var t = Math.round(Math.random() * 1000);
  console.log('ASAD TIME', t);
  setTimeout(function() {
    cb(a + b, t);
  }, t);
}

as.add(asad, 5, 2, function(r, t) {
  console.log('Result:', r, 'Time:', t);
});

as.add(asad, 3, 4);

as.run(0);
as.run(1);