(function (global) {
  "use strict";
  
  //Input object class Task -> Function and Array of one or more arguments
  function Task() {
    var fa = arguments[0]; //first argument
    //validate arguments -> length > 0
    if (arguments.length === 0) {
      throw new TypeError('Task must have minimal one parameter!');
    }
    //validate first argument
    if(typeof fa !== 'function' &&
      (typeof fa !== 'object' || fa.constructor.name !== 'Array')) {
      throw new TypeError('Task first argument must be Function or Array!');
    }
    
    //Multi input (Array iteration) task
    if (typeof fa === 'object' && fa.constructor.name === 'Array') {
      if (typeof arguments[1] !== 'function') {
        throw new TypeError('Task function must be defined in second argument!');
      }
      this.args = arguments[0]; //Array of values to iterate
      this.fn = arguments[1]; //Task fn -> iterator
      this.length = this.args.length;
      //callback is in arguments[2]
      if (arguments.length > 2 && typeof arguments[2] === 'function') {
        this.cb = arguments[2]; //arguments index > 2 is not relevant..
      }
    //Single function task
    } else if (typeof fa === 'function') {
      this.fn = arguments[0];
      var arlen;
      //determine number of arguments without last callback function
      if (arguments.length > 1 && typeof arguments[arguments.length - 1] === 'function') {
        this.cb = arguments[arguments.length - 1];
        arlen = arguments.length - 2; // - fn and cb
      } else { //cb not found
        arlen = arguments.length - 1; // - fn
      }
      
      //multiple arguments
      if (arlen > 1) {
        this.args = {};
        for (var i = 0; i < arlen; i++) {
          this.args[i] = arguments[i + 1];
        }
        this.args.length = i + 1; //length of args Object
      //single one argument
      } else {
        this.args = arguments[1];
        this.args.length = 1;
      }
      this.length = 1;
    }
  }
  
  //One Async (parallel) instance 
  function As(opts) {
    //options
    this.options = (typeof opts === 'object') ? opts : {};
    this.options.limit = parseInt(this.options.limit, 10) > 0 ? this.options.limit : 100;
    this.options.errStop = (this.options.errStop === 'true') ? true : false;
    
    //callback run when this As is done
    this.final = null;
    
    //parent / child
    this.parent = null;
    this.child = null;
    
    //status
    this.idx = [0, 0]; //index and subindex of last started task
    this.done = [0, 0]; //number of done tasks
    
    this.tasks = []; //tasks
    this.results = []; //results
  }
  
  As.prototype.add = function add() {
    var task = new Task(arguments);
    this.tasks.push(task);
    return this;
  };
  
  As.prototype.getRunning = function getRunning() {
    //return this.idx - this.done;
    var doidx = this.done[0]; //index of last done
    var idx = this.idx[0]; //index of task
    var dosidx = this.done[1]; //subindex of last done
    var sidx = this.idx[1]; //index of subtask
    
    var running = 0;
    
    if ((idx - doidx) > 0) {
      //diff done subindex to next index (doidx + 1)
      running += (this.tasks[doidx].length - 1) - dosidx;
      //full indexes without last (idx)
      for (var i = doidx + 1; i < idx; i++) {
        running += this.tasks[i].length;
      }
    }
    //number of last subtasks (sidx + 1)
    running += sidx + 1;
    
  };
  
  As.prototype.loger = function loger(idxar, msg) {
    //log
    console.log(msg, idxar[0] + 1,'. task of', this.tasks.length, idxar[1] + 1,
      '. subtask of', this.tasks[idxar[0]].length, 'running:', this.getRunning(),
      'done:', this.done);
  };

  //save result and run cb if any (idx is [idx, sidx])
  As.prototype.ucb = function ucb(idxar) {
    var tidx = idxar[0];
    var stidx = idxar[1];
    var task = this.tasks[tidx]; //one task
    var fn = task.fn; //task fn
    var args = task.args[stidx]; //subtask (one input arguments)
    var cb = task.cb; //own task callback, run if any found
  
    //run own callback if any
    var self = this;
    if (typeof cb === 'function') {
      cb.apply(self, args);
    }
    
    //save result to this.results array
    this.saveResult(idxar);
    
    //one is done
    this.done++;
    //log done event
    this.loger(idxar, '<-DONE->');
    
    //check next tasks or done
    this.runNext();
  };
  
  
  
  //check if next task found or done actions
  As.prototype.runNext = function runNext() {
    //recursion or done if last index num.
    var last_idx = this.tasks.length - 1;
    var last_subidx = this[last_idx].length - 1;
    var last = [last_idx, last_subidx];
    if(((this.done[0] < last[0]) || (this.done[1] < last[1])) &&
      (this.getRunning() > 0)) {
      this.run();
    } else if(((this.idx === this.items.length && this.idx[1] === this.done[1])) &&
      this.getRunning() === 0) {
      this.done = this.idx = [0, 0]; //reset
      this.final(this.results);
    }
  };

  As.prototype.run_one = function run_one(fn, args, idxar) {
    //TODO args neni 1 arg ale pole argunebtů
    //parsovat a vstriknout original cb, kdyz je...
    this.loger(idxar, '<-STARTED->');
    var self = this;
    //run fn with universal callback
    fn(args, function() {
      self.ucb(arguments, idxar);
    });
  };

  As.prototype.run = function run() {
    //fill parallell run buffer
    while((this.idx < this.items.length) && (this.getRunning() <= this.limit)) {
      var args = this.items[this.idx];
      this.run_one(mult, args, this.idx);
      this.idx++;
    }
  };
  
  global.As = As;
})(this);

/////////////////////////////////////////////////////////////////////
//Task se udela jako
//
/*global As*/
function mult(arg, callback) {
  //var rt = arg * 100;
  var rt = Math.round(Math.random() * 500);
  console.log('Process of \''+arg+'\', arg and return after ' + rt + ' sec.');
  setTimeout(function() {
    callback(arg * 2); 
  }, rt);
}
var args = [ 1, 2, 3, 4, 5, 6 ];
function final(results) { console.log('Done all:', results); }

var as = new As(mult, args, 3, final);
as.run();