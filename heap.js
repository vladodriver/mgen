(function() {
  
  function Heap(ar, cfn, type) {
    //parse argument by type
    for (var n = 0; n < 2; n++) {
      var arg = arguments[n];
      switch (typeof arg) {
        case 'object':
          if (arg.length && arg.length > 0) {
            this.har = arg;
          }
          break;
        case 'function':
          this.cfn = arg;
          break;
        case 'string':
          if(arg === 'min' || arg === 'max') {
            this.type = arg;
          }
          break;
      }
    }
    
    this.har = this.har || [];
    this.cfn = this.cfn || function(x) {return x}; //fallback
    this.type = this.type || 'min';
    if (this.har.length > 1) {
      this.heapify(this.har, this.type);
    }
    //console.log(this);
    return this;
  }
  
  //swap elements in ar at index ai and bi
  Heap.prototype._swap = function(ai, bi, ar) {
    var tmp = ar[ai];
    ar[ai] = ar[bi];
    ar[bi] = tmp;
  };
  
  Heap.prototype.shiftUp = function shiftUp(mini, i, ar, type) {
    var pi, parent, child, parentN, childN;
    
    while (i > mini) {
      child = ar[i];  //element (child)
      pi = ((i - 1) >> 1); //parent index
      parent = ar[pi]; //parent element
      
      childN = this.cfn(child); //child numeric value
      parentN = this.cfn(parent); //parent numeric value
      
      if((childN < parentN && type === 'min') ||
        (childN > parentN && type === 'max')) {
        //swap child node at index i <-> parent
        this._swap(i , pi, ar);
        i = pi;  
      } else {
        break; //done
      }
    }
  };
  
  Heap.prototype.shiftDown = function shiftDown(i, maxi, ar, type) {
    //var len = ar.length;
    var li, ri, parent, parentN, left, leftN, right, rightN;
    
    while (i < maxi) {
      parent = ar[i]; //element at i index is parent
      parentN = this.cfn(parent); //num. value of parent
      li = (2 * i) + 1; //left child index
      ri = (2 * i) + 2; //right child index
      left = ar[li];
      leftN = this.cfn(left);
      right = ar[ri];
      rightN = this.cfn(right);
      
      //validate left child node if os on heap array range
      var swi = null; //need index of swap el.
      if (li <= maxi) {
        if((leftN < parentN && type === 'min') ||
          (leftN > parentN && type === 'max')) {
          swi = li;
        }
      }
      //validate right child node if is in heap array range
      if(ri <= maxi) {
        //compare to leftN if si != null, or compare parent
        var coN = swi ? leftN : parentN;
        if ((rightN < coN && type === 'min') ||
          (rightN > coN && type === 'max')) {
          swi = ri;
        }
      }
      //swap invalid parent to child of minimal num. value
      if (swi) {
        this._swap(i, swi, ar);
        i = swi;
      } else { //if is done
        break;
      }
    }
  };
  
  Heap.prototype.push = function push(el) {
    var len = this.har.push(el);//add new el to end
    this.shiftUp(0, len - 1, this.har, this.type); //reorder
    return this.har;
  };
  
  Heap.prototype.pop = function pop() {
    var root = this.har[0]; //get root to return
    var end = this.har.pop(); //cut last from har
    
    if (this.har.length > 0) {
      //insert last place of first -> to root
      this.har[0] = end;
      //reorder
      this.shiftDown(0, end, this.har, this.type);
    }
    return root;
  };
  
  Heap.prototype.remidx = function remidx(i) {
    var end = this.har.length - 1, rm;
    if(this.har.length > 0 && i < this.har.length) {
      this._swap(end, i, this.har);
      rm = this.har.pop(); //removed el
      end--;
      this.shiftDown(i, end, this.har, this.type);
      //this.shiftUp(0, i, this.har, this.type);
    }
    return rm || false;
  };
  
  Heap.prototype.remove = function remove(el) {
    var idx = this.har.indexOf(el);
    var rm;
    if (idx !== -1) {
      rm = this.remidx(idx);
    }
    return rm || false;
  };
  
  Heap.prototype.heapify = function heapify(ar, type) {
    type = (type === 'min' || type === 'max') ? type : 'max';
    var maxi = ar.length - 1; //end -> last ar element index
    //index of element 1/2 array position
    var hi = ar.length >> 1;
    while(hi >= 0) {
      this.shiftDown(hi, maxi, ar, type);
      hi--;
    }
    return ar;
  };
  
  Heap.prototype.sort = function(ar, type) {
    type = (type === 'min' || type === 'max') ? type : 'max';
    var li = ar.length - 1; //last ar el
    ar = this.heapify(ar, type); //first heapify full ar
    
    while (li > 0) {
      //swap root (0) and li index
      this._swap(0, li, ar); //swap first end
      li--; //cut ar and repair heap
      this.shiftDown(0, li, ar, type);
    }
    return ar;
  };
  
  //exports..
  if (typeof window === 'object') { //browser
    this.Heap = Heap;
  } else if (typeof module === 'object') { //nodejs
    module.exports = Heap;
  }

})();

function ra(n, max) {
  var out = [];
  for (var i = 0; i < n; i++) {
    out.push(Math.round(Math.random() * (max || 10000))); 
  }
  return out;
}