module.exports = {
  AudioVideo: {
    aliases: ['Multimedia', 'Sound & Video'],
    subs: ['Audio', 'Database', 'Video', 'Sound & Video', 'HamRadio', 'Midi', 'Mixer', 'Sequencer',
      'Tuner', 'TV', 'AudioVideoEditing', 'Player', 'Recorder', 'DiscBurning', 'Music']
  },
  Development: {
    aliases: ['Programming'],
    subs: ['Building', 'Database', 'Debugger', 'IDE', 'GUIDesigner', 'Profiling', 'ProjectManagement',
      'RevisionControl', 'Translation', 'WebDevelopment']
  },
  Education: {
    aliases: [],
    subs: ['Art', 'Construction', 'Music', 'Languages', 'ArtificialIntelligence', 'Astronomy', 'Biology',
      'Chemistry', 'ComputerScience', 'DataVisualization', 'Economy', 'Electricity', 'Geography',
      'Geology', 'Geoscience', 'History', 'Humanities', 'ImageProcessing', 'Literature', 'Maps', 'Math',
      'NumericalAnalysis', 'MedicalSoftware', 'Physics', 'Robotics', 'Spirituality', 'Sports',
      'ParallelComputing']
  },
  Game: {
    aliases: ['Games'],
    subs: ['ActionGame', 'AdventureGame', 'ArcadeGame', 'BoardGame', 'BlocksGame', 'CardGame', 'Emulator',
      'KidsGame', 'LogicGame', 'RolePlaying', 'Shooter', 'Simulation', 'SportsGame', 'StrategyGame']
  },
  Graphics: {
    aliases: [],
    subs: ['2DGraphics', 'VectorGraphics', 'RasterGraphics', '3DGraphics', 'Scanning', 'OCR',
      'Photography', 'Publishing', 'Viewer']
  },
  Network: {
    aliases: ['Internet', 'Web Applications'],
    subs: ['Email', 'Dialup', 'InstantMessaging', 'Chat', 'IRCClient', 'Feed', 'FileTransfer',
      'HamRadio', 'News', 'P2P', 'RemoteAccess', 'Telephony', 'VideoConference', 'WebBrowser',
      'WebDevelopment', 'Monitor']
  },
  Office: {
    aliases: [],
    subs: ['Calendar', 'ContactManagement', 'Database', 'Dictionary', 'Chart', 'Email', 'Finance',
      'FlowChart', 'Photography', 'PDA', , 'Presentation', 'ProjectManagement', 'Publishing',
      'Spreadsheet', 'Viewer', 'WordProcessor']
  },
  Science: {
    aliases: ['Science & Math'],
    subs: ['Art', 'Construction', 'Languages', 'ArtificialIntelligence', 'Astronomy', 'Biology',
      'Chemistry', 'ComputerScience', 'DataVisualization', 'Economy', 'Electricity', 'Geography',
      'Geology', 'Geoscience', 'History', 'Humanities', 'ImageProcessing', 'Literature', 'Maps',
      'Math', 'NumericalAnalysis', 'MedicalSoftware', 'Physics', 'Robotics', 'Spirituality', 
      'Sports', 'ParallelComputing']
  },
  Settings: {
    aliases: ['Preferences'],
    subs: ['Accessibility', 'DesktopSettings', 'HardwareSettings', 'Printing', 'PackageManager', 'Security']
  },
  System: {
    aliases: ['Accessories', 'System Settings', 'System Tools'],
    subs: ['Emulator', 'FileTools', 'FileManager', 'Filesystem', 'TerminalEmulator', 'Monitor',
    'Security']
  },
  Utility: {
    aliases: ['Control Center'],
    subs: ['Accessibility', 'Archiving', 'Calculator', 'Clock', 'Compression', 'FileTools', 'TextTools',
      'TelephonyTools', 'Maps', 'Spirituality', 'TextEditor']
  }
};